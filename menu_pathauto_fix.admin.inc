<?php

/**
 * @file
 * Drupal API functions for the Module Admin Settings.
 */

/**
 *
 */
function menu_pathauto_fix_admin_settings() {
  $content_types = array_keys(node_type_get_types());
  $content_type_options = array();
  foreach ($content_types as $type) {
    $content_type_options[$type] = $type;
  }

  $form['content_types'] = array(
    '#type' => 'fieldset',
    '#title' => t('Content Types'),
  );

  $form['content_types']['selected_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t("When using the pathauto module to generate URL aliases for your content, it won't automatically update your content URL's automatically if you move their location in the menu via the menu admin. Select the content types you want to enable the hierachial path alias fix for."),
    '#options' => $content_type_options,
    '#default_value' => variable_get('menu_pathauto_fix_content_types', array()),
  );

  $menus = array_keys(menu_get_menus(TRUE));
  foreach ($menus as $menu_name) {
    $menu_options[$menu_name] = $menu_name;
  }

  $form['menus'] = array(
    '#type' => 'fieldset',
    '#title' => t('Enabled Menus'),
  );

  $form['menus']['enabled_menus'] = array(
    '#type' => 'checkboxes',
    '#title' => t("Which menus do you want to activate this module for."),
    '#options' => $menu_options,
    '#default_value' => variable_get('menu_pathauto_fix_menus', array()),
  );

  $form['debug'] = array(
    '#type' => 'fieldset',
    '#title' => t('Debug Setting'),
  );
  $form['debug']['watchdog_logging'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable verbose logging of the menu items effected and the URL alias details'),
    '#default_value' => variable_get('menu_pathauto_fix_debug', 0),
  );

  return system_settings_form($form);
}

/**
 *
 */
function menu_pathauto_fix_admin_settings_validate($form, $form_state) {
  $content_types = array();
  foreach ($form_state['values']['selected_types'] as $key => $value) {
    if (!empty($value)) {
      $content_types[$key] = check_plain($value);
    }
  }

  $menus = array();
  foreach ($form_state['values']['enabled_menus'] as $key => $value) {
    if (!empty($value)) {
      $menus[$key] = check_plain($value);
    }
  }

  variable_set('menu_pathauto_fix_debug', check_plain($form_state['values']['watchdog_logging']));
  variable_set('menu_pathauto_fix_content_types', $content_types);
  variable_set('menu_pathauto_fix_menus', $menus);
}
